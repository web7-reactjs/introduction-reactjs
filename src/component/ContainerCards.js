import * as React from "react";
import {Col, Row} from 'react-bootstrap'
import CustomCard from "./CustomCard";

export class ContainerCards extends React.Component {
   constructor(props){
    super( props)
    this.state={
        title : " Mythical Animale",
        description: " this animal is very rare , You can not find it with your own eyes",
        action : "Watch the video "
    }
   }
 
  render() {
    return (
      <div>

        <h1>{this.props.developerName}</h1>
        <h1>{this.state.title}</h1>
        <h1>{this.state.description}</h1>
        <h1>{ this. state.action}</h1>

        <div className="container">
          <Row>
            <Col
              md={4}
              className="text-center d-flex justify-content-center mb-5"
            >
              <CustomCard authorname =  "Mr. Rainbow man" title={this.state.title}  description = {this.state.description} action = {this.state.action} />
            </Col>
            
          </Row>
        </div>
      </div>
    );
  }
}

export default ContainerCards;
