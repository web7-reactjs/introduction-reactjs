import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';

function CustomCard(props)  {

  let isConditionValid= true ;  
  return (
    

    <Card style={{ width: '18rem' }}>
      <Card.Img variant="top" src="https://i.pinimg.com/originals/85/a1/4d/85a14d0ef1502b3d33a3510b60cbe8d6.jpg" />
      <Card.Body>
     
        <Card.Title>  {props.title} </Card.Title>
        <Card.Text>
          {props.description}
        </Card.Text>
        <Button variant="primary"> {props.action}</Button>
      </Card.Body>

      
    </Card>

     
  );
}

export default CustomCard;