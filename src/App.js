import "./App.css";
import { Button, Row, Col } from "react-bootstrap";
import CustomCard from "./component/CustomCard";
import CustomNavBar from "./component/CustomNavBar";
import "bootstrap/dist/css/bootstrap.min.css";
import CustomFooter from "./component/CustomFooter";
import ContainerCards from "./component/ContainerCards";
function App() {
  return (
    <>
      <CustomNavBar />
      <ContainerCards developerName = "Mr. John Snow"/>

      <CustomFooter />
    </>
  );
}

export default App;
